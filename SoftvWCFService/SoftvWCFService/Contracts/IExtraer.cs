﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace SoftvWCFService.Contracts
{
    [ServiceContract]
    public interface IExtraer
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "EnviarMensaje", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        String Enviar();

    }
}
