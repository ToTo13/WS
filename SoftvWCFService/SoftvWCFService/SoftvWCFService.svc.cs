﻿
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using SoftvWCFService.Contracts;

namespace SoftvWCFService
{
    [ScriptService]
    public partial class SoftvWCFService : IExtraer
    {
        public String Enviar()
        {
            var Parametros = HttpContext.Current.Request;
            string Numero = Parametros["msisdns"].ToString();
            string Mensaje = Parametros["message"].ToString();
            string Tag = Parametros["tag"].ToString();
            string respuesta = "";

            const string WEBSERVICE_URL = "https://api.broadcastermobile.com/brdcstr-endpoint-web/services/messaging/";
            try
            {
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    string postData = "{\"apiKey\":\"982\",\"dial\":\"26262\",\"country\":\"MX\",\"tag\":\"" + Tag + "\",\"message\":\"" + Mensaje + "\",\"msisdns\":\"52" + Numero + "\"}";
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    webRequest.Method = "POST";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", "c8YFTL59N05k8agab53UikwULrk=");

                    Stream dataStream = webRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                    WebResponse response = webRequest.GetResponse();

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            respuesta = (string.Format("Response: {0}", jsonResponse));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return (ex.ToString());
            }
            finally
            {
                string ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    /********************* Ejecutar el comando *********************/
                    String sql = "exec GuardarDatos @Tag=" + Tag + ",@Mensaje='" + Mensaje + "', @Numero=" + Numero + ", @Respuesta=" + respuesta + ";";
                    //return sql;
                    SqlCommand command = new SqlCommand(sql, connection);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                }
            }
            return respuesta;

        }
       
    }
}