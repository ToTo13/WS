﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace SoftvWCFService.Contracts
{
    interface IEii
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "EnviarMensaje", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        String Enviar();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "EnviarGrind", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        String SendGrind();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "EnviarTwilio", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        String Twilio();
    }
}
