﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace SoftvWCFService.Contracts
{
    [ServiceContract]
    public interface IExtraer
    {
        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Prueba", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        String Prueba();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "RadioSAT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        String AldeamoRadioSAT();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "ClickHD", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        String AldeamoClickHD();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Cybernet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        String AldeamoCybernet();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Comunet", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        String AldeamoComunet();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Cablexpand", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        String AldeamoCablexpand();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Zafiro", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        String AldeamoZafiro();

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "Mastv", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        String AldeamoMastv();

    }
}
