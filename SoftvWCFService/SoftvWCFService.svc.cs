﻿
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Xml.Linq;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using SoftvWCFService.Contracts;

namespace SoftvWCFService
{
    [ScriptService]
    public partial class SoftvWCFService : IEii, IExtraer
    {
        public String Prueba()
        {
            return "Servicio Funcionando";
        }

        public String Enviar()
        {
            var Parametros = HttpContext.Current.Request;
            String Numero = Parametros["msisdns"].ToString();
            String Mensaje = Parametros["message"].ToString();
            String Tag = Parametros["tag"].ToString();
            String respuesta = "";

            const String WEBSERVICE_URL = "https://api.broadcastermobile.com/brdcstr-endpoint-web/services/messaging/";
            try
            {
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    String postData = "{\"apiKey\":\"982\",\"dial\":\"26262\",\"country\":\"MX\",\"tag\":\"" + Tag + "\",\"message\":\"" + Mensaje + "\",\"msisdns\":\"52" + Numero + "\"}";
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    webRequest.Method = "POST";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", "c8YFTL59N05k8agab53UikwULrk=");

                    Stream dataStream = webRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();
                    WebResponse response = webRequest.GetResponse();

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            respuesta = (String.Format("Response: {0}", jsonResponse));
                            return respuesta;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return (ex.ToString());
            }
            finally
            {
                String ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    /********************* Ejecutar el comando *********************/
                    String sql = "exec GuardarDatos @Tag='" + Tag + "',@Mensaje='" + Mensaje + "', @Numero='" + Numero + "', @Respuesta='" + respuesta + "';";
                    //return sql;
                    SqlCommand command = new SqlCommand(sql, connection);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                }
            }
            return respuesta;

        }

        public String SendGrind()
        {
            var Parametros = HttpContext.Current.Request;
            String Correo = Parametros["correo"].ToString();
            String Asunto = Parametros["asunto"].ToString();
            String Mensaje = Parametros["message"].ToString();
            String respuesta = "";

            const String WEBSERVICE_URL = "https://api.sendgrid.com/v3/mail/send";

            try
            {
                String postData = "{\"personalizations\":[{\"to\":[{\"email\":\"" + Correo + "\"}],\"subject\":\"" + Asunto + "\"}],\"from\":{\"email\":\"toto130297@gmail.com\"},\"content\":[{\"type\":\"text/html\",\"value\":\"<!DOCTYPEhtml><html><head></head><body><h2>Titulo</h2><p>" + Mensaje + "</p></body></html>\"}]}";

                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    webRequest.Method = "POST";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", "Bearer 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");

                    Stream dataStream = webRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    WebResponse response = webRequest.GetResponse();

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            respuesta = (String.Format("Response: {0}", jsonResponse));
                            //return respuesta;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return (ex.ToString());
            }
            finally
            {
                String ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    /********************* Ejecutar el comando *********************/
                    String sql = "exec GuardarDatosSendGrind @Mensaje='" + Mensaje + "', @Correo='" + Correo + "', @Asunto='" + Asunto + "', @Respuesta='" + respuesta + "';";
                    //return sql;
                    SqlCommand command = new SqlCommand(sql, connection);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                }
            }

            return respuesta;
        }

        public String Twilio()
        {  
            String respuesta = "Twilio";
            var Parametros = HttpContext.Current.Request;
            String Numero = Parametros["msisdns"].ToString();
            String Mensaje = Parametros["message"].ToString();
            String Tag = Parametros["tag"].ToString();

            //const string accountSid = "ACc5a1401a95ac36a1a8df0bd8703f124e";
            //const string authToken = "7233d135b395c45ae62f4450468921f8";

            //TwilioClient.Init(accountSid, authToken);

            //try
            //{
            //    var message = MessageResource.Create(
            //        body: "This is the ship that made the Kessel Run in fourteen parsecs?",
            //        from: new Twilio.Types.PhoneNumber("+12728081903"),
            //        to: new Twilio.Types.PhoneNumber("+524491799494")
            //    );
            //    respuesta = message.Sid;
            //}
            //catch (Exception ex)
            //{
            //    return (ex.ToString());
            //}           

            return respuesta;
        }

        public String Aldeamo()
        {
            var Parametros = HttpContext.Current.Request;
            String Numero = Parametros["msisdns"].ToString();
            String Mensaje = Parametros["message"].ToString();
            String respuesta = "";

            const String WEBSERVICE_URL = "https://apitellit.aldeamo.com/SmsiWS/smsSendPost/";
            const String username = "FullSolution";
            const String password = "Ful15oLlu123*";
            
            try
            {

                string authInfo = username + ":" + password;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                //return encoded;
                String postData = "{\"country\": \"57\",\"message\":\"" + Mensaje + "\",\"messageFormat\": 0,\"addresseeList\": [{\"mobile\": \"" + Numero + "\"}]}";
                //return postData;
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    webRequest.Method = "POST";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", "Basic " + authInfo);
                    
                    Stream dataStream = webRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    WebResponse response = webRequest.GetResponse();

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            respuesta = (String.Format("Response: {0}", jsonResponse));
                            //return respuesta;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return (ex.ToString());
            }
            finally
            {
                String ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    /********************* Ejecutar el comando *********************/
                    String sql = "exec GuardarDatos @Mensaje='" + Mensaje + "', @Numero='" + Numero + "', @Respuesta='" + respuesta + "';";
                    //return sql;
                    SqlCommand command = new SqlCommand(sql, connection);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                }
            }

            return respuesta;

        }

        public String AldeamoRadioSAT()
        {
            var Parametros = HttpContext.Current.Request;
            String Numero = Parametros["msisdns"].ToString();
            String Mensaje = Parametros["message"].ToString();
            String respuesta = "";

            const String WEBSERVICE_URL = "https://apitellit.aldeamo.com/SmsiWS/smsSendPost/";
            const String username = "FullSolution";
            const String password = "Ful15oLlu123*";

            try
            {

                string authInfo = username + ":" + password;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                //return encoded;
                String postData = "{\"country\": \"57\",\"message\":\"" + Mensaje + "\",\"messageFormat\": 0,\"addresseeList\": [{\"mobile\": \"" + Numero + "\"}]}";
                //return postData;
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    webRequest.Method = "POST";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", "Basic " + authInfo);

                    Stream dataStream = webRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    WebResponse response = webRequest.GetResponse();

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            respuesta = (String.Format("Response: {0}", jsonResponse));
                            //return respuesta;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return (ex.ToString());
            }
            finally
            {
                String ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionStringRadioSAT"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    /********************* Ejecutar el comando *********************/
                    String sql = "exec GuardarDatos @Mensaje='" + Mensaje + "', @Numero='" + Numero + "', @Respuesta='" + respuesta + "';";
                    //return sql;
                    SqlCommand command = new SqlCommand(sql, connection);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                }
            }

            return respuesta;

        }

        public String AldeamoClickHD()
        {
            var Parametros = HttpContext.Current.Request;
            String Numero = Parametros["msisdns"].ToString();
            String Mensaje = Parametros["message"].ToString();
            String respuesta = "";

            const String WEBSERVICE_URL = "https://apitellit.aldeamo.com/SmsiWS/smsSendPost/";
            const String username = "FullSolution";
            const String password = "Ful15oLlu123*";

            try
            {

                string authInfo = username + ":" + password;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                //return encoded;
                String postData = "{\"country\": \"57\",\"message\":\"" + Mensaje + "\",\"messageFormat\": 0,\"addresseeList\": [{\"mobile\": \"" + Numero + "\"}]}";
                //return postData;
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    webRequest.Method = "POST";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", "Basic " + authInfo);

                    Stream dataStream = webRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    WebResponse response = webRequest.GetResponse();

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            respuesta = (String.Format("Response: {0}", jsonResponse));
                            //return respuesta;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return (ex.ToString());
            }
            finally
            {
                String ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionStringClickHD"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    /********************* Ejecutar el comando *********************/
                    String sql = "exec GuardarDatos @Mensaje='" + Mensaje + "', @Numero='" + Numero + "', @Respuesta='" + respuesta + "';";
                    //return sql;
                    SqlCommand command = new SqlCommand(sql, connection);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                }
            }

            return respuesta;

        }

        public String AldeamoCybernet()
        {
            var Parametros = HttpContext.Current.Request;
            String Numero = Parametros["msisdns"].ToString();
            String Mensaje = Parametros["message"].ToString();
            String respuesta = "";

            const String WEBSERVICE_URL = "https://apitellit.aldeamo.com/SmsiWS/smsSendPost/";
            const String username = "FullSolution";
            const String password = "Ful15oLlu123*";

            try
            {

                string authInfo = username + ":" + password;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                //return encoded;
                String postData = "{\"country\": \"57\",\"message\":\"" + Mensaje + "\",\"messageFormat\": 0,\"addresseeList\": [{\"mobile\": \"" + Numero + "\"}]}";
                //return postData;
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    webRequest.Method = "POST";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", "Basic " + authInfo);

                    Stream dataStream = webRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    WebResponse response = webRequest.GetResponse();

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            respuesta = (String.Format("Response: {0}", jsonResponse));
                            //return respuesta;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return (ex.ToString());
            }
            finally
            {
                String ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionStringCybernet"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    /********************* Ejecutar el comando *********************/
                    String sql = "exec GuardarDatos @Mensaje='" + Mensaje + "', @Numero='" + Numero + "', @Respuesta='" + respuesta + "';";
                    //return sql;
                    SqlCommand command = new SqlCommand(sql, connection);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                }
            }

            return respuesta;

        }

        public String AldeamoComunet()
        {
            var Parametros = HttpContext.Current.Request;
            String Numero = Parametros["msisdns"].ToString();
            String Mensaje = Parametros["message"].ToString();
            String respuesta = "";

            const String WEBSERVICE_URL = "https://apitellit.aldeamo.com/SmsiWS/smsSendPost/";
            const String username = "FullSolution";
            const String password = "Ful15oLlu123*";

            try
            {

                string authInfo = username + ":" + password;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                //return encoded;
                String postData = "{\"country\": \"57\",\"message\":\"" + Mensaje + "\",\"messageFormat\": 0,\"addresseeList\": [{\"mobile\": \"" + Numero + "\"}]}";
                //return postData;
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    webRequest.Method = "POST";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", "Basic " + authInfo);

                    Stream dataStream = webRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    WebResponse response = webRequest.GetResponse();

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            respuesta = (String.Format("Response: {0}", jsonResponse));
                            //return respuesta;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return (ex.ToString());
            }
            finally
            {
                String ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionStringComunet"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    /********************* Ejecutar el comando *********************/
                    String sql = "exec GuardarDatos @Mensaje='" + Mensaje + "', @Numero='" + Numero + "', @Respuesta='" + respuesta + "';";
                    //return sql;
                    SqlCommand command = new SqlCommand(sql, connection);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                }
            }

            return respuesta;

        }

        public String AldeamoCablexpand()
        {
            var Parametros = HttpContext.Current.Request;
            String Numero = Parametros["msisdns"].ToString();
            String Mensaje = Parametros["message"].ToString();
            String respuesta = "";

            const String WEBSERVICE_URL = "https://apitellit.aldeamo.com/SmsiWS/smsSendPost/";
            const String username = "FullSolution";
            const String password = "Ful15oLlu123*";

            try
            {

                string authInfo = username + ":" + password;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                //return encoded;
                String postData = "{\"country\": \"57\",\"message\":\"" + Mensaje + "\",\"messageFormat\": 0,\"addresseeList\": [{\"mobile\": \"" + Numero + "\"}]}";
                //return postData;
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    webRequest.Method = "POST";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", "Basic " + authInfo);

                    Stream dataStream = webRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    WebResponse response = webRequest.GetResponse();

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            respuesta = (String.Format("Response: {0}", jsonResponse));
                            //return respuesta;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return (ex.ToString());
            }
            finally
            {
                String ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionStringCablexpand"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    /********************* Ejecutar el comando *********************/
                    String sql = "exec GuardarDatos @Mensaje='" + Mensaje + "', @Numero='" + Numero + "', @Respuesta='" + respuesta + "';";
                    //return sql;
                    SqlCommand command = new SqlCommand(sql, connection);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                }
            }

            return respuesta;

        }

        public String AldeamoZafiro()
        {
            var Parametros = HttpContext.Current.Request;
            String Numero = Parametros["msisdns"].ToString();
            String Mensaje = Parametros["message"].ToString();
            String respuesta = "";

            const String WEBSERVICE_URL = "https://apitellit.aldeamo.com/SmsiWS/smsSendPost/";
            const String username = "FullSolution";
            const String password = "Ful15oLlu123*";

            try
            {

                string authInfo = username + ":" + password;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                //return encoded;
                String postData = "{\"country\": \"57\",\"message\":\"" + Mensaje + "\",\"messageFormat\": 0,\"addresseeList\": [{\"mobile\": \"" + Numero + "\"}]}";
                //return postData;
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    webRequest.Method = "POST";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", "Basic " + authInfo);

                    Stream dataStream = webRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    WebResponse response = webRequest.GetResponse();

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            respuesta = (String.Format("Response: {0}", jsonResponse));
                            //return respuesta;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return (ex.ToString());
            }
            finally
            {
                String ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionStringZafiro"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    /********************* Ejecutar el comando *********************/
                    String sql = "exec GuardarDatos @Mensaje='" + Mensaje + "', @Numero='" + Numero + "', @Respuesta='" + respuesta + "';";
                    //return sql;
                    SqlCommand command = new SqlCommand(sql, connection);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                }
            }

            return respuesta;

        }

        public String AldeamoMastv()
        {
            var Parametros = HttpContext.Current.Request;
            String Numero = Parametros["msisdns"].ToString();
            String Mensaje = Parametros["message"].ToString();
            String respuesta = "";

            const String WEBSERVICE_URL = "https://apitellit.aldeamo.com/SmsiWS/smsSendPost/";
            const String username = "FullSolution";
            const String password = "Ful15oLlu123*";

            try
            {

                string authInfo = username + ":" + password;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                //return encoded;
                String postData = "{\"country\": \"57\",\"message\":\"" + Mensaje + "\",\"messageFormat\": 0,\"addresseeList\": [{\"mobile\": \"" + Numero + "\"}]}";
                //return postData;
                var webRequest = System.Net.WebRequest.Create(WEBSERVICE_URL);
                if (webRequest != null)
                {
                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    webRequest.Method = "POST";
                    webRequest.Timeout = 12000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", "Basic " + authInfo);

                    Stream dataStream = webRequest.GetRequestStream();
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    WebResponse response = webRequest.GetResponse();

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            respuesta = (String.Format("Response: {0}", jsonResponse));
                            //return respuesta;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return (ex.ToString());
            }
            finally
            {
                String ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionStringMastv"].ConnectionString;
                using (SqlConnection connection = new SqlConnection(ConnectionString))
                {
                    /********************* Ejecutar el comando *********************/
                    String sql = "exec GuardarDatos @Mensaje='" + Mensaje + "', @Numero='" + Numero + "', @Respuesta='" + respuesta + "';";
                    //return sql;
                    SqlCommand command = new SqlCommand(sql, connection);
                    IDataReader rd = null;
                    try
                    {
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();
                        rd = command.ExecuteReader();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error " + ex.Message, ex);
                    }
                    finally
                    {
                        if (connection != null)
                            connection.Close();
                        if (rd != null)
                            rd.Close();
                    }
                }
            }

            return respuesta;

        }
    }
}